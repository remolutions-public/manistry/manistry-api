FROM node:18-alpine as builder

COPY . ./

ENV BUILD_DIR=/app/src/app/

COPY . ${BUILD_DIR}

WORKDIR ${BUILD_DIR}

RUN npm install
RUN npm run build


##### actual image #####
FROM node:18-alpine

ENV NODE_ENV=production \
    NPM_CONFIG_PREFIX=/home/node/.npm-global \
    PATH=$PATH:/home/node/.npm-global/bin:/home/node/node_modules/.bin:$PATH \
    BUILD_DIR=/app/src/app/

RUN apk add --update openssl curl \
    && rm -rf /var/cache/apk/* \
    && mkdir -p /var/api/node_modules \
    && mkdir -p /var/api/secrets \
    && chown -R node:node /var/api

USER node

WORKDIR /var/api

COPY --from=builder --chown=node:node ${BUILD_DIR}/package*.json ./

RUN npm i --prod \
    && npm cache verify

COPY --from=builder --chown=node:node ${BUILD_DIR}/secrets/create-jwt.sh ./secrets/create-jwt.sh
COPY --from=builder --chown=node:node ${BUILD_DIR}/dist ./dist
COPY --from=builder --chown=node:node ${BUILD_DIR}/entrypoint.sh ./entrypoint.sh

CMD [ "sh", "entrypoint.sh" ]
