# What is this?
A specialized api, to manage the docker registry.



# How to prune all docker images
```shell
docker system prune -af --volumes

```


# Quickstart
- create a copy of the .env.EXAMPLE file and name it .env.dev
  - set values for the recaptcha vars
- create a users.json file in the secrets folder with this content:
```json
{"test":{"username":"test","password":"test","isadmin":true}}
```


# Registry API
- https://registry-k8s-io.remolutions.com/v2/_catalog
- https://docs.docker.com/registry/spec/api/
- https://github.com/SUSE/Portus
- https://github.com/goharbor/harbor



# Test dockerize and upload an image to local registry
```shell
sh test/container/test-dockerize.sh

```
