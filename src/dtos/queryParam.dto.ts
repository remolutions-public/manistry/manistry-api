import { ApiProperty } from '@nestjs/swagger';

export class LimitOffsetQueryDto {
  @ApiProperty({
    description: 'Limit amount returned records',
    minimum: 1,
    required: false
  })
  limit?: number;

  @ApiProperty({
    description: 'Set page offset',
    minimum: 0,
    default: 0,
    required: false
  })
  offset?: number;

  @ApiProperty({
    description: 'Search for specific pattern',
    required: false
  })
  filter?: string;

  @ApiProperty({
    description: 'What should be sorted by which order',
    required: false
  })
  orderBy?: string[];
}


