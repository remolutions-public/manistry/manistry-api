import { ApiProperty } from '@nestjs/swagger';

export class UserDto {
  @ApiProperty({
    description: 'Username',
    required: true
  })
  username: string;

  @ApiProperty({
    description: 'Password',
    required: false
  })
  password?: string;

  @ApiProperty({
    description: 'Tells the system if this user is an admin',
    required: false
  })
  isadmin?: boolean;

  @ApiProperty({
    description: 'Last user login',
    required: false
  })
  lastLogin?: string;
}


