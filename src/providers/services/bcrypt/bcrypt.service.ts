import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class BcryptService {
  private salt: any = bcrypt.genSaltSync(BcryptService.rounds);
  private hash: any;
  private static rounds: number = 10;

  public getHash(plain: string): string {
    if (plain == undefined) {
      return '';
    }
    this.hash = bcrypt.hashSync(plain, this.salt);
    return this.hash;
  }

  public verify(plain: string, hash: string): boolean {
    if (plain == undefined || hash == undefined || !this.isValidHash(hash)) {
      return false;
    }
    return bcrypt.compareSync(plain, hash);
  }

  public checkHash(plain: string, hash: string) {
    return this.verify(plain, hash);
  }

  public isValidHash(pw: string): boolean {
    if (pw == undefined) {
      return false;
    }
    return pw.startsWith('$2a$' + BcryptService.rounds + '$');
  }
}
