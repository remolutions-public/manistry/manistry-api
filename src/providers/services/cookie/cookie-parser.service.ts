export class CookieParserService {
  private static cookieInstance: CookieParserService;

  constructor() {
    if (!CookieParserService.cookieInstance) {
      CookieParserService.cookieInstance = this;
    }
  }

  static getInst(): CookieParserService {
    return CookieParserService.cookieInstance || new CookieParserService();
  }

  public parse = function(req: any, res: any, next: any): void {
    if (req['headers'] && req['headers']['cookie']) {
      let rawCookies = req['headers']['cookie'];
      try {
        rawCookies = req['headers']['cookie'].replace(/["']/g, '');
      } catch(e) {}
      const cookies = rawCookies.split(';').filter((entry: any) => entry.length);
      const extracted = {};
      cookies.forEach((cookie: any) => {
        const keyVal = cookie.trim().split('=');
        extracted[keyVal[0]] = keyVal[1];
      });
      req['cookies'] = extracted;
    }
  }
}
