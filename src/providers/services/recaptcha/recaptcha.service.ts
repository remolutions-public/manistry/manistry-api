import { Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { GlobalConfigService } from '../config/config.service';
import { JwtService } from '../jwt/jwt.service';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class RecaptchaService {
  private mode = this.globalConfigService.getValue('ENV');
  private publicRecaptchaKey = this.globalConfigService.getValue(
    'RECAPTCHA_PUBLIC_KEY',
  );
  private privateRecaptchaKey = this.globalConfigService.getValue(
    'RECAPTCHA_PRIVATE_KEY',
  );
  private recaptchaVerifyAddress = this.globalConfigService.getValue(
    'RECAPTCHA_VERIFY_ADDRESS',
  );

  constructor(
    private readonly globalConfigService: GlobalConfigService,
    private readonly http: HttpService,
    private readonly logger: Logger,
    private readonly jwtService: JwtService,
  ) {}

  /**
   *
   * @param req
   * @param token
   */
  async verifyRecaptcha(req: any, token: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (!(token == undefined)) {
        if (this.mode !== 'DEV') {
          if (this.privateRecaptchaKey == undefined || !this.privateRecaptchaKey.length) {
            return reject(new Error('No recaptcha private key provided!'));
          }
          const request = {
            secret: this.privateRecaptchaKey,
            response: token,
            remoteip: this.jwtService.getOriginIp(req),
          };

          await firstValueFrom(
            this.http.post(
              this.recaptchaVerifyAddress +
                '?secret=' +
                request['secret'] +
                '&response=' +
                request['response'] +
                '&remoteip=' +
                request['remoteip'],
              request,
            ),
          )
            .then((res) => {
              resolve(res['data']['success']);
            })
            .catch((err) => reject(err));
        } else {
          resolve(true);
        }
      } else {
        reject(new Error('no recaptcha token provided!'));
      }
    });
  }
}
