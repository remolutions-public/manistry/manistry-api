import { Injectable, Logger, HttpException } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { GlobalConfigService } from '../config/config.service';
import * as path from 'path';
import * as fs from 'fs';
import { exec } from 'child_process';
import { debounce } from '../../../libraries/debounce.lib';

@Injectable()
export class JwtService {
  private loggerContext = 'JwtService';
  private secretsPath = path.join(process.cwd(), 'secrets');
  private jwtPath = path.join(this.secretsPath, 'jwt');
  private privKey: string;
  private pubKey: string;
  private privKeyName = this.globalConfigService.getValue('JWT_PRIV_KEY');
  private pubKeyName = this.globalConfigService.getValue('JWT_PUB_KEY');
  private apiMode = this.globalConfigService.getValue('ENV');
  private tokenPrefix = this.globalConfigService.getValue('TOKEN_PREFIX');

  private jwtPrivOpts: jwt.SignOptions = {
    algorithm: 'ES256',
    expiresIn: '168h', // 7 days
    // frontend tries refresh if below 6 days rest duration
  };
  private jwtPrivOptsCsrf: jwt.SignOptions = {
    algorithm: 'ES256',
    expiresIn: '216h', // 9 days
  };
  private jwtPrivOptsUnlimited: jwt.SignOptions = {
    algorithm: 'ES256',
  };
  private jwtPubOpts: jwt.VerifyOptions = {
    algorithms: ['ES256'],
  };

  authTokenPrefix = this.tokenPrefix + '-auth';
  csrfTokenPrefix = this.tokenPrefix + '-csrf';

  constructor(
    private readonly globalConfigService: GlobalConfigService,
    private readonly logger: Logger,
  ) {
    this.initJwtService();
  }

  async initJwtService() {
    debounce('initJwtService', 250, async () => {
      if (!fs.existsSync(this.jwtPath)) {
        fs.mkdirSync(this.jwtPath, { recursive: true });
      }

      if (!fs.existsSync(path.join(this.jwtPath, this.privKeyName))) {
        await new Promise((resolve) => {
          exec(
            `sh ${path.join(this.secretsPath, 'create-jwt.sh')}`,
            (error, stdout, stderr) => {
              if (!(error == undefined)) {
                this.logger.error(
                  error,
                  undefined,
                  this.loggerContext + ' - init - error',
                );
                return resolve(undefined);
              }
              if (
                !(stderr == undefined) &&
                stderr.length &&
                stderr.toLowerCase().indexOf('error') > -1
              ) {
                this.logger.error(
                  stderr,
                  undefined,
                  this.loggerContext + ' - init - stderr',
                );
                return resolve(undefined);
              }
              this.logger.debug('created new jwt secrets! ' + stdout);
              resolve(undefined);
            },
          );
        });
      }

      this.privKey = fs
        .readFileSync(path.join(this.jwtPath, this.privKeyName))
        .toString();
      this.pubKey = fs
        .readFileSync(path.join(this.jwtPath, this.pubKeyName))
        .toString();
    });
  }

  /**
   *
   * @param token
   */
  public simpleBase64Decode(token: string) {
    return jwt.decode(token);
  }

  /**
   * Build an unlimited JWT for generic auth purposes
   *
   */
  public makeUnlimitedToken(obj: any): Promise<string> {
    return new Promise((resolve, reject) => {
      jwt.sign(
        obj,
        this.privKey,
        this.jwtPrivOptsUnlimited,
        (err: Error, token: any) => {
          if (err) reject(err);
          else resolve(token);
        },
      );
    });
  }

  /**
   * Build JWT for user session
   *
   */
  public makeAuthToken(user: any): Promise<any> {
    return new Promise((resolve, reject) => {
      jwt.sign(
        {
          username: user.username,
          isauthtoken: true,
        },
        this.privKey,
        this.jwtPrivOpts,
        (err: Error, token: any) => {
          if (err) reject(err);
          else resolve(token);
        },
      );
    });
  }

  /**
   *
   * @param obj
   */
  public makeCsrfToken(obj: any): Promise<any> {
    return new Promise((resolve, reject) => {
      obj['iscsrftoken'] = true;
      jwt.sign(
        obj,
        this.privKey,
        this.jwtPrivOptsCsrf,
        (err: Error, token: any) => {
          if (err) reject(err);
          else resolve(token);
        },
      );
    });
  }

  /**
   * Verify and decode JWT
   *
   */
  async verifyAuth(req: any): Promise<any> {
    if (this.hasAuthToken(req)) {
      let token = this.extractToken(req);

      // DEBUGGING
      // this.logger.debug(token, 'token - verifyAuth #0');

      token = this.matchCsrfToAuthToken(token);

      // DEBUGGING
      // this.logger.debug(token, 'token - verifyAuth #1');

      if (token?.auth == undefined) {
        throw new HttpException('Was not able to match tokens', 403);
      }
      let errorState: any;
      const decodedAuth = await this.verify(token?.auth).catch((err) => {
        errorState = err;
      });

      // DEBUGGING
      // this.logger.debug(decodedAuth, 'decodedAuth - verifyAuth #2');

      if (!(errorState == undefined)) {
        throw new HttpException(errorState, 500);
      }
      if (!decodedAuth) {
        throw new HttpException('No auth token provided', 403);
      }
      const decodedCsrf = await this.verify(token['csrf']).catch((err) => {
        errorState = err;
      });

      // DEBUGGING
      // this.logger.debug(decodedCsrf, 'decodedCsrf - verifyAuth #3');

      if (!(errorState == undefined)) {
        throw {
          error: errorState,
          code: 500,
        };
      }
      if (!decodedCsrf) {
        throw new HttpException('No csrf token provided', 403);
      }
      // attach extracted and matched cookie tokens to req object
      req['tokens'] = token;
      const resultObj = {
        auth: decodedAuth,
        csrf: decodedCsrf,
      };

      // DEBUGGING
      // this.logger.debug(resultObj, 'resultObj - verifyAuth #4');

      return resultObj;
    } else {
      throw new HttpException('Has no auth token', 420);
    }
  }

  /**
   * Verify and decode a single JWT
   *
   */
  async verify(token: string): Promise<any> {
    return new Promise((resolve, reject) => {
      jwt.verify(
        token,
        this.pubKey,
        this.jwtPubOpts,
        (err: Error, decoded: any) => {
          if (err) {
            return reject({
              error: err,
              message: 'token is invalid',
            });
          }

          resolve(decoded);
        },
      );
    });
  }

  // ##########################################################################
  // ########################### Helper Functions #############################
  // ##########################################################################
  /**
   *
   * @param req
   */
  private extractToken(req: any) {
    let token: any;
    if (this.hasAuthToken(req)) {
      token = {
        auth: this.filterAuthTokens(req),
      };


      // DEBUGGING
      // this.logger.debug(req.headers, 'req.headers - extractToken #0');
      // this.logger.debug(req.cookies, 'req.cookies - extractToken #1');
      // this.logger.debug(req.query, 'req.query - extractToken #2');


      if (!(req.headers?.[this.csrfTokenPrefix] == undefined)) {
        token['csrf'] = req.headers[this.csrfTokenPrefix];
      } else if (!(req.headers?.[this.csrfTokenPrefix] == undefined)) {
        token['csrf'] = req.headers[this.csrfTokenPrefix];
      } else if (!(req.cookies?.[this.csrfTokenPrefix] == undefined)) {
        token['csrf'] = req.cookies[this.csrfTokenPrefix];
      } else if (!(req.cookies?.[this.csrfTokenPrefix] == undefined)) {
        token['csrf'] = req.cookies[this.csrfTokenPrefix];
      } else if (!(req.query?.[this.csrfTokenPrefix] == undefined)) {
        token['csrf'] = req.query[this.csrfTokenPrefix];
      } else if (!(req.query?.[this.csrfTokenPrefix] == undefined)) {
        token['csrf'] = req.query[this.csrfTokenPrefix];
      }
    }
    return token;
  }

  /**
   *
   * @param cookies
   */
  private hasAuthToken(req: any): boolean {
    if (!(req?.cookies == undefined)) {
      for (let key of Object.keys(req?.cookies)) {
        if (key.indexOf(this.authTokenPrefix) > -1) {
          return true;
        }
      }
    } else if (!(req?.headers == undefined)) {
      for (let key of Object.keys(req?.headers)) {
        if (key.indexOf(this.authTokenPrefix) > -1) {
          return true;
        }
      }
    }
    return false;
  }

  /**
   *
   * @param cookies
   */
  private filterAuthTokens(req: any) {
    let authTokens = {};
    if (!(req?.cookies == undefined)) {
      for (let key of Object.keys(req?.cookies)) {
        if (key.indexOf(this.authTokenPrefix) > -1) {
          authTokens[key] = req?.cookies[key];
        }
      }
    } else if (!(req?.headers?.[this.authTokenPrefix] == undefined)) {
      for (let key of Object.keys(req?.headers)) {
        if (key.indexOf(this.authTokenPrefix) > -1) {
          authTokens[key] = req?.headers[key];
        }
      }
    }
    return authTokens;
  }

  /**
   *
   * @param token
   */
  private matchCsrfToAuthToken(token: any) {
    if (token['csrf'] && token['auth']) {
      const decodedCsrf = jwt.decode(token['csrf']);
      const auth = token.auth;
      let retToken: any;
      for (let key of Object.keys(auth)) {
        const compKey = this.authTokenPrefix + '-' + decodedCsrf['username'];
        if (key === compKey) {
          retToken = {
            auth: auth[key],
            csrf: token.csrf,
          };
          break;
        }
      }
      if (retToken == undefined) {
        if (!(token?.auth?.[this.authTokenPrefix] == undefined)) {
          retToken = {
            auth: token?.auth?.[this.authTokenPrefix],
            csrf: token.csrf,
          };
        }
      }
      return retToken;
    }
  }

  /**
   *
   * @param req
   */
  getOriginPort(req: any): string {
    return req.get('host') ? req.get('host').split(':')[1] : '';
  }

  /**
   *
   * @param req
   */
  getOriginUrl(req: any): string {
    return req.get('host') ? req.get('host').split(':')[0] : '';
  }

  /**
   *
   * @param req
   */
  getRoute(req: any): string {
    return req['path']
      ? req['path']
      : req['originalUrl']
      ? req['originalUrl']
      : '';
  }

  /**
   *
   * @param req
   */
  getOriginProtocol(req: any): string {
    return req.protocol;
  }

  /**
   *
   * @param req
   */
  getOriginIp(req: any): string {
    return (
      req.headers['x-forwarded-for'] ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress ||
      (req.connection.socket
        ? req.connection.socket.remoteAddress
        : 'undefined')
    );
  }

  /**
   *
   * @param req
   */
  getUsername(req: any): string {
    let username = req['params'] ? req['params']['username'] : undefined;
    if (!username)
      username = req['query'] ? req['query']['username'] : undefined;
    if (!username) username = req['body'] ? req['body']['username'] : undefined;
    return username;
  }
}
