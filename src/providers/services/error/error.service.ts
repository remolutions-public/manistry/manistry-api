import { Injectable, HttpException, HttpStatus } from '@nestjs/common';

@Injectable()
export class ErrorService {
  private static errorServiceInstance: ErrorService;

  constructor() {
    if (!ErrorService.errorServiceInstance) {
      ErrorService.errorServiceInstance = this;
    }
  }

  static getInst(): ErrorService {
    return ErrorService.errorServiceInstance || new ErrorService();
  }

  /**
   * Get data from reporting url
   *
   * @param {Object} err - Error object to report
   * @param {Number} status - Optional http status code
   */
  public handle = function(err: Error | any, status?: number): void {
    let error = err;
    let code = status;
    if (err['error']) {
      error = err['error'];
      if (err['status']) {
        code = err['status'];
      }
    } else if (err['response'] && err['response']['data']) {
      error = err['response']['data'];
      code =
        err['response']['data']['statusCode'] ||
        err['response']['data']['status'] ||
        err['response']['data']['code'];
    }
    throw new HttpException(error['message'], code ? code : HttpStatus.INTERNAL_SERVER_ERROR);
  }

  public prep = function(err: Error | any, status?: number) {
    let error = err;
    let code = status;
    if (err['error']) {
      error = err['error'];
      if (err['status']) {
        code = err['status'];
      }
    }
    return {
      err: error['message'],
      status: code ? code : HttpStatus.INTERNAL_SERVER_ERROR,
    };
  }
}
