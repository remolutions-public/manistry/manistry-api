import * as dotenv from 'dotenv';
import * as path from 'path';

export const sourceEnv = () => {
  if (process.env.ENV === 'DEV' || process.env.ENV == undefined) {
    dotenv.config({ path: path.join(__dirname, '../../.env.dev') });
  }

  return {
    ENV: process.env.ENV || 'DEV',
    APP_PORT: process.env.APP_PORT,
    API_VERSION: process.env.API_VERSION,
    REGISTRY_HOSTS: process.env.REGISTRY_HOSTS,
    TOKEN_PREFIX: process.env.TOKEN_PREFIX,
    DO_UPDATE_USERS: process.env.DO_UPDATE_USERS || "false",
    BASIC_AUTH_USERNAME: process.env.BASIC_AUTH_USERNAME,
    BASIC_AUTH_PASSWORD: process.env.BASIC_AUTH_PASSWORD,
    JWT_PRIV_KEY: process.env.JWT_PRIV_KEY,
    JWT_PUB_KEY: process.env.JWT_PUB_KEY,
    RECAPTCHA_PRIVATE_KEY: process.env.RECAPTCHA_PRIVATE_KEY,
    RECAPTCHA_PUBLIC_KEY: process.env.RECAPTCHA_PUBLIC_KEY,
    RECAPTCHA_VERIFY_ADDRESS: process.env.RECAPTCHA_VERIFY_ADDRESS,
  };
};

export const envVars = sourceEnv();
