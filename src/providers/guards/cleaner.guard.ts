import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class CleanerGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.clearHeaderInjections(request);
  }

  clearHeaderInjections(req?: any): boolean {
    if (req["isAuth"]) {
      delete req["isAuth"];
    }
    if (req["isIdMatch"]) { // is Owner
      delete req["isIdMatch"];
    }
    if (req["isAdmin"]) {
      delete req["isAdmin"];
    }
    if (req['decoded_tokens']) {
      delete req["decoded_tokens"];
    }
    return true;
  }
}
