import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { JwtService } from '../services/jwt/jwt.service';
import { Observable } from 'rxjs';
import { Logger } from '@nestjs/common';

@Injectable()
export class AuthGuard implements CanActivate {
  logger = new Logger('auth.guard.ts');

  constructor(private readonly jwtService: JwtService) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateJwt(request);
  }

  async validateJwt(req: any): Promise<boolean> {
    return new Promise(async (resolve) => {
      const token = (await this.jwtService.verifyAuth(req).catch((err) => {
        this.logger.error(err);
      })) as any;

      if (!token) {
        return resolve(false);
      }

      req['decoded_tokens'] = token;
      return resolve(true);
    });
  }
}
