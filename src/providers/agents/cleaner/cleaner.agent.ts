import { Injectable, Logger } from '@nestjs/common';
import { Cron, SchedulerRegistry } from '@nestjs/schedule';
import { GlobalConfigService } from '../../services/config/config.service';
import { sleep } from '../../../libraries/sleep.lib';

@Injectable()
export class CleanerAgent {
  isDebugging = false;
  private loggerContext = 'CleanerAgent';
  isAgentRunning = false;
  lastAgentStartTime: number;
  maxAllowedAgentRunDelay = 60 * 1000;

  constructor(
    private readonly schedulerRegistry: SchedulerRegistry,
    private readonly logger: Logger,
    private readonly configService: GlobalConfigService,
  ) {
    // init upon api start, don't wait for first run
    setTimeout(async () => {
      this.runAgent();
    }, 500);
  }

  @Cron('*/15 * * * * *', {
    name: 'cleaner_agent_cron_job',
  })
  async runAgent(): Promise<void> {
    if (!this.isAgentRunning) {
      this.isAgentRunning = true;
      await sleep(Math.random() * 2450 + 50);
      this.lastAgentStartTime = new Date().valueOf();
      if (this.isDebugging) {
        this.logger.debug(
          new Date().toISOString() + ' cleaner_agent_cron_job is running...',
          this.loggerContext,
        );
      }

      // TODO

      this.finishRunningAgentCycle();
    }
  }

  stopAgent(): void {
    const job = this.schedulerRegistry.getCronJob('cleaner_agent_cron_job');
    job.stop();
    this.logger.debug('Stopped cleaner agent cron job.', this.loggerContext);
  }

  startAgent(): void {
    const job = this.schedulerRegistry.getCronJob('cleaner_agent_cron_job');
    job.start();
    this.logger.debug(
      'Cleaner agent cron job was started.',
      this.loggerContext,
    );
  }

  restartAgent(): void {
    this.stopAgent();
    this.startAgent();
  }

  finishRunningAgentCycle() {
    if (this.isDebugging) {
      this.logger.debug(
        new Date().toISOString() +
          ' cleaner_agent_cron_job has finished. (exec time: ' +
          (new Date().valueOf() - this.lastAgentStartTime) +
          ' ms)',
        this.loggerContext,
      );
    }

    this.isAgentRunning = false;
  }
}
