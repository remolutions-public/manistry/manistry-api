import { Module, Logger } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { GlobalProvidersModule } from '../global-providers.module';

import { CleanerAgent } from './cleaner/cleaner.agent';

const SERVICES = [
  Logger,
  CleanerAgent,
];

const MODULES = [
  HttpModule,
  GlobalProvidersModule,
];

@Module({
  imports: [...MODULES],
  providers: [...SERVICES],
  exports: [...SERVICES],
})
export class AgentModule {}
