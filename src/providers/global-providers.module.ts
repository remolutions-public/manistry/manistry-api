import { Module, Logger } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { ScheduleModule } from '@nestjs/schedule';
import { APP_GUARD } from '@nestjs/core';
import { ApiVersionGuard } from './guards/api-versions';
import { AuthGuard } from './guards/auth.guard';
import { ConfigService } from '@nestjs/config';
import { ErrorService } from './services/error/error.service';
import { GlobalConfigService } from './services/config/config.service';
import { JwtService } from './services/jwt/jwt.service';
import { BcryptService } from './services/bcrypt/bcrypt.service';
import { RecaptchaService } from './services/recaptcha/recaptcha.service';


const SERVICES = [
  Logger,
  ErrorService,
  ConfigService,
  GlobalConfigService,
  JwtService,
  BcryptService,
  RecaptchaService,
];

const GUARDS = [
  {
    provide: APP_GUARD,
    useClass: ApiVersionGuard,
  },
  AuthGuard,
];

const MODULES = [
  HttpModule,
  ScheduleModule.forRoot()
];

@Module({
  imports: [...MODULES],
  providers: [...SERVICES, ...GUARDS],
  exports: [...SERVICES],
})
export class GlobalProvidersModule {}
