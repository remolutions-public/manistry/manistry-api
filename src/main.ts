import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { CleanerGuard } from './providers/guards/cleaner.guard';
import { CookieParserService } from './providers/services/cookie/cookie-parser.service';
import { GlobalConfigService } from './providers/services/config/config.service';
import { json, urlencoded } from 'body-parser';

const logger = new Logger('main.ts');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const cookieParser = new CookieParserService();
  const sizeLimit = '500mb';
  const globalConfigService: GlobalConfigService = app.get(GlobalConfigService);
  const port = globalConfigService.getPort();

  app.enableCors({
    origin: true,
    allowedHeaders: '*',
    methods: 'GET,PATCH,PUT,POST,DELETE,OPTIONS',
    credentials: true,
    // preflightContinue: false,
    // optionsSuccessStatus: 204,
  });

  const config = new DocumentBuilder()
    .setTitle('Manistry')
    .setDescription('Managed Container Image Registry API')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  app.useGlobalGuards(new CleanerGuard());
  app.use((req: any, res: any, next: any) => {
    cookieParser.parse(req, res, next);
    next();
  });
  app.setGlobalPrefix('api/:v');

  app.use(json({ limit: sizeLimit }));
  app.use(urlencoded({ limit: sizeLimit, extended: true }));

  await app.listen(port);
  logger.debug(`api started and listening on: 0.0.0.0:${port}/`);
}
bootstrap();
