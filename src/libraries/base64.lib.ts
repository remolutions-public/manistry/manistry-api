// encode
export const atob = (text: string) => {
  return Buffer.from(text).toString('base64');
};

// decode
export const btoa = (b64Encoded: string) => {
  return Buffer.from(b64Encoded, 'base64').toString();
};
