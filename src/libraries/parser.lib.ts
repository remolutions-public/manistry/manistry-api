import { btoa, atob } from './base64.lib';

export const getExpectedValue = (data: string) => {
  let expectedValue = 0;
  if (!(data == undefined) && data.length) {
    try {
      const decodedData = dataParse(data);
      if (!(decodedData == undefined) && !Number.isNaN(decodedData.value)) {
        expectedValue = parseFloat(decodedData.value || 0);
      }
    } catch (e) {
      console.error(e);
    }
  }
  return expectedValue;
};

export const dataParse = (data: string) => {
  if (!(data == undefined) && data.length) {
    try {
      return JSON.parse(btoa(data));
    } catch (e) {
      console.error(e);
    }
  }
};

export const encodeData = (dataObj: any): string => {
  if (!(dataObj == undefined)) {
    try {
      let stringifiedData = dataObj;
      if (typeof dataObj === 'object') {
        stringifiedData = JSON.stringify(dataObj);
      }
      return atob(stringifiedData);
    } catch (e) {
      console.error(e);
    }
  }
};

export const parseBoolean = (input: any): boolean => {
  if (
    !(input == undefined) &&
    (input === 1 || input === 'true' || input === true)
  ) {
    return true;
  } else {
    return false;
  }
};

export const typeIsBoolean = (input: any): boolean => {
  if (
    input === 'true' ||
    input === 'false' ||
    input === true ||
    input === false
  ) {
    return true;
  }
  return false;
};

export const typeIsNumeric = (input: any): boolean => {
  if (input == undefined || Number.isNaN(input)) {
    return false;
  }
  try {
    const parsedInput = parseFloat(input);
    if (Number.isNaN(parsedInput)) {
      return false;
    }
    if (typeof parsedInput === 'number') {
      return true;
    }
    return false;
  } catch (e) {
    return false;
  }
};

export const typeIsArray = (input: any): boolean => {
  if (Array.isArray(input)) {
    return true;
  }
  return false;
};

export const typeIsObject = (input: any): boolean => {
  if (typeof input === 'object') {
    return true;
  }
  return false;
};

export const typeIsString = (input: any): boolean => {
  if (!typeIsBoolean(input) && !typeIsNumeric(input)) {
    if (typeof input === 'string') {
      return true;
    }
  }
  return false;
};

export const typeIsJSON = (input: any): boolean => {
  if (typeIsString(input)) {
    if (input.charAt(0) === '[' || input.charAt(0) === '{') {
      return true;
    }
  }
  return false;
};

export const parseSettingsValues = (mergedSetting: any): any => {
  let value: any = {};

  if (mergedSetting.defaultValue) {
    if (typeIsJSON(mergedSetting.defaultValue)) {
      Object.assign(value, JSON.parse(mergedSetting.defaultValue));
    } else {
      if (typeIsBoolean(mergedSetting.defaultValue)) {
        value = parseBoolean(mergedSetting.defaultValue);
      } else if (typeIsNumeric(mergedSetting.defaultValue)) {
        value = parseFloat(mergedSetting.defaultValue);
      } else {
        value = mergedSetting.defaultValue;
      }
    }
  }
  if (mergedSetting.value) {
    if (typeIsJSON(mergedSetting.value)) {
      Object.assign(value, JSON.parse(mergedSetting.value));
    } else {
      if (typeIsBoolean(mergedSetting.value)) {
        value = parseBoolean(mergedSetting.value);
      } else if (typeIsNumeric(mergedSetting.value)) {
        value = parseFloat(mergedSetting.value);
      } else {
        value = mergedSetting.value;
      }
    }
  }
  return value;
}