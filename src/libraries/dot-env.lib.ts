import * as fs from 'fs';
import * as path from 'path';

export const getEnvVar = (key: string) => {
  const envFilePath = path.join(process.cwd(), '.env.dev');
  if (fs.existsSync(envFilePath)) {
    const fileContentBuffer = fs.readFileSync(envFilePath);
    if (fileContentBuffer == undefined) {
      return;
    }
    const fileContent = fileContentBuffer.toString();
    const contentSplit = fileContent.split(key + '=');
    if (contentSplit[1] == undefined) {
      return;
    }
    const value = contentSplit[1].split('\n')[0];
    return value;
  }
};
