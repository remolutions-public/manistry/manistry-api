// decode
export const copyObj = (obj: any): any => {
  if (!(obj == undefined)) {
    return JSON.parse(JSON.stringify(obj));
  }
};
