export function padTo2Digits(num: number) {
  return num.toString().padStart(2, '0');
}

export function formatDate_ddMMyyyy(date: number | Date) {
  date = new Date(date);
  return [
    padTo2Digits(date.getDate()),
    padTo2Digits(date.getMonth() + 1),
    date.getFullYear(),
  ].join('/');
}

export function formatUsDate(date: number | Date) {
  date = new Date(date);
  return [
    padTo2Digits(date.getMonth() + 1),
    padTo2Digits(date.getDate()),
    date.getFullYear(),
  ].join('/');
}



export function formatISODate(date: number | Date) {
  date = new Date(date);
  return [
    date.getFullYear(),
    padTo2Digits(date.getMonth() + 1),
    padTo2Digits(date.getDate()),
  ].join('-');
}