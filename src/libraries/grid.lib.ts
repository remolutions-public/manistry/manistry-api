const gridWidthCodes = {
  0: 'A',
  1: 'B',
  2: 'C',
  3: 'D',
  4: 'E',
  5: 'F',
  6: 'G',
  7: 'H',
  8: 'I',
  9: 'J',
  10: 'K',
  11: 'L',
  12: 'M',
  13: 'N',
  14: 'O',
  15: 'P',
  16: 'Q',
  17: 'R',
  18: 'S',
  19: 'T',
  20: 'U',
  21: 'V',
  22: 'W',
  23: 'X',
  24: 'Y',
  25: 'Z',
  26: 'AA',
  27: 'AB',
};

export const translateGridCodeToGridCoords = (grid: string) => {
  if (grid === 'DK') {
    // is DevKit
    return { gridX: 0, gridY: 0 };
  } else if (grid === 'BW') {
    // is Blackwood
    return { gridX: 0, gridY: 0 };
  } else if (grid === 'SSW') {
    // is SingleServerWorld
    return { gridX: 0, gridY: 0 };
  }
  const gridY = grid.replace(/\D/g, '');
  const gridXCode = grid.replace(gridY, '');
  let gridX: number;
  for (const index of Object.keys(gridWidthCodes)) {
    if (gridWidthCodes[index] === gridXCode) {
      gridX = parseInt(index, 10);
      break;
    }
  }
  return {
    gridX,
    gridY: parseInt(gridY, 10) - 1,
  };
};

export const translateGridCoordsToGridCode = (gridX: number, gridY: number) => {
  return '' + gridWidthCodes[gridX] + (gridY + 1);
};
