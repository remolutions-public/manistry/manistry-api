export const sleep = async (delay?: number) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(undefined);
    }, delay || 10);
  });
};
