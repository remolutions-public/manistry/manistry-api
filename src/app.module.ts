import { Module, Logger } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GlobalProvidersModule } from './providers/global-providers.module';
import { envVars } from './providers/environment';
import { AgentModule } from './providers/agents/agent.module';
import { HealthCheckModule } from './routes/healthcheck/healthcheck.module';
import { RegistryModule } from './routes/registry/registry.module';
import { AuthModule } from './routes/auth/auth.module';

const MODULES = [
  GlobalProvidersModule,
  ConfigModule.forRoot({
    isGlobal: true,
    envFilePath: '.env.' + envVars.ENV.toLowerCase(),
  }),
  AgentModule,
  // routes
  HealthCheckModule,
  RegistryModule,
  AuthModule,
];

const SERVICES = [AppService, Logger];

@Module({
  imports: [...MODULES],
  controllers: [AppController],
  providers: [...SERVICES],
})
export class AppModule {}
