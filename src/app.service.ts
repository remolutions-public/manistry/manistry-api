import { Injectable, Logger } from '@nestjs/common';
import { GlobalConfigService } from './providers/services/config/config.service';

@Injectable()
export class AppService {
  constructor(
    private readonly logger: Logger,
    private readonly configService: GlobalConfigService
  ) {}

  getVersion(): any {
    return { version: this.configService.getValue('API_VERSION') };
  }
}
