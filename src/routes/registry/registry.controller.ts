import { Controller, Get, Delete, Query, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ApiVersion } from '../../providers/guards/api-versions';
import { RegistryService } from './registry.service';
import { ErrorService } from '../../providers/services/error/error.service';
import { LimitOffsetQueryDto } from '../../dtos/queryParam.dto';
import { AuthGuard } from '../../providers/guards/auth.guard';

@ApiTags('registry')
@Controller('registry')
export class RegistryController {
  constructor(
    private readonly registryService: RegistryService,
    private readonly errorService: ErrorService,
  ) {}

  @Get()
  @ApiVersion()
  @UseGuards(AuthGuard)
  async getCatalog(@Query() query: LimitOffsetQueryDto): Promise<any> {
    return this.registryService.getCatalog(query);
  }

  @Get('tags')
  @ApiVersion()
  @UseGuards(AuthGuard)
  async getTags(@Query() query: any): Promise<any> {
    return this.registryService.getTags(query);
  }

  @Get('manifests')
  @ApiVersion()
  @UseGuards(AuthGuard)
  async getManifests(@Query() query: any): Promise<any> {
    return this.registryService.getManifests(query);
  }

  @Delete()
  @ApiVersion()
  @UseGuards(AuthGuard)
  async deleteImage(@Query() query: any): Promise<any> {
    return this.registryService.deleteImage(query);
  }
}
