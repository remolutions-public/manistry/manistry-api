import { HttpException, Injectable, Logger } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { LimitOffsetQueryDto } from '../../dtos/queryParam.dto';
import { GlobalConfigService } from '../../providers/services/config/config.service';
import { firstValueFrom } from 'rxjs';
import {
  typeIsArray,
  typeIsJSON,
  typeIsString,
  parseBoolean,
} from '../../libraries/parser.lib';
import * as fs from 'fs';
import * as path from 'path';

@Injectable()
export class RegistryService {
  private loggerContext = 'RegistryService';

  private registryHosts = this.configService
    .getValue('REGISTRY_HOSTS')
    ?.split(',');

  private registryBasicAuthUsername = this.configService.getValue(
    'BASIC_AUTH_USERNAME',
  );
  private registryBasicAuthPassword = this.configService.getValue(
    'BASIC_AUTH_PASSWORD',
  );

  // 15 minutes cache
  private maxTagCacheTime = 15 * 60 * 1000;
  private cachedTagLists = {};

  private httpCallTimeout = 15000;

  constructor(
    private readonly logger: Logger,
    private readonly configService: GlobalConfigService,
    private readonly httpService: HttpService,
  ) {}

  async getCatalog(query: LimitOffsetQueryDto): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const limit = parseInt((query.limit || 0) as any, 10);
      const offset = parseInt((query.offset || 0) as any) * limit;
      const filter = query.filter;
      const orderBy = query.orderBy;
      let errorState: any;

      const raceRequests = [];
      for (const host of this.getShuffledRegistryHosts()) {
        raceRequests.push(
          firstValueFrom(
            this.httpService.get(`${host}/v2/_catalog`, {
              withCredentials: true,
              timeout: this.httpCallTimeout,
              auth: {
                username: this.registryBasicAuthUsername,
                password: this.registryBasicAuthPassword,
              },
            }),
          ),
        );
      }
      const result = (await Promise.any(raceRequests).catch((err) => {
        errorState = err;
      })) as any;
      if (!(errorState == undefined)) {
        this.logger.error(
          errorState.response?.data?.errors || errorState,
          undefined,
          this.loggerContext + ' - get catalog',
        );
        return reject(new HttpException('Could not get catalog', 500));
      }

      if (result?.data?.repositories == undefined) {
        return reject(new HttpException('No registry data found', 404));
      }

      let catalog = [];
      for (const entry of result.data?.repositories) {
        catalog.push({ name: entry });
      }

      catalog = this.filterList(catalog, filter);
      const rows = catalog.length;
      if (orderBy == undefined) {
        catalog = this.sortList(catalog, 'name', 'ASC');
      } else {
        catalog = this.sortList(catalog, orderBy[0], orderBy[1]);
      }

      if (limit > 0) {
        catalog = catalog.slice(offset, offset + limit);
      }

      resolve({ catalog, rows });
    });
  }

  async getTags(query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const registryName = query.name;
      if (registryName == undefined || !registryName.length) {
        return reject(new HttpException('no registry name/path provided', 420));
      }

      const limit = parseInt((query.limit || 0) as any, 10);
      const offset = parseInt((query.offset || 0) as any) * limit;
      const filter = query.filter;
      const orderBy = query.orderBy;
      const cachedOnly = parseBoolean(query.cached);
      const forceCacheRefresh = parseBoolean(query.force);

      if (
        this.cachedTagLists[registryName] == undefined ||
        new Date().valueOf() - this.cachedTagLists[registryName].timestamp >
          this.maxTagCacheTime ||
        forceCacheRefresh
      ) {
        let errorState: any;
        const raceRequests = [];
        for (const host of this.getShuffledRegistryHosts()) {
          raceRequests.push(
            firstValueFrom(
              this.httpService.get(
                host + '/v2/' + registryName + '/tags/list',
                {
                  withCredentials: true,
                  timeout: this.httpCallTimeout,
                  auth: {
                    username: this.registryBasicAuthUsername,
                    password: this.registryBasicAuthPassword,
                  },
                },
              ),
            ),
          );
        }
        const result = (await Promise.any(raceRequests).catch((err) => {
          errorState = err;
        })) as any;
        if (!(errorState == undefined)) {
          this.logger.error(
            errorState.response?.data?.errors || errorState,
            undefined,
            this.loggerContext + ' - get tags',
          );
          return reject(new HttpException('Failed to get tags', 500));
        }
        if (result?.data == undefined) {
          return reject(new HttpException('No tags found', 404));
        }
        this.cachedTagLists[registryName] = {
          timestamp: new Date().valueOf(),
          data: result.data,
        };
      }

      // TESTING
      // console.info(this.cachedTagLists);

      const registryObj = this.cachedTagLists[registryName];
      const name = registryObj.data?.name;

      let tags = [];
      if (!(registryObj.data?.tags == undefined)) {
        for (const tag of registryObj.data?.tags) {
          tags.push({ tag });
        }
      }

      const localTags = this.getLocallyCachedTags(registryName);
      if (cachedOnly) {
        const newTags = [];
        for (const entry of tags) {
          if (!(localTags[entry.tag] == undefined)) {
            entry.stat = localTags[entry.tag];
            newTags.push(entry);
          }
        }
        tags = newTags;
      } else {
        for (const entry of tags) {
          if (!(localTags[entry.tag] == undefined)) {
            entry.stat = localTags[entry.tag];
          } else {
            const resultObj = await this.getTagHistory({
              name: registryName,
              tag: entry.tag,
            }).catch((err) => {
              this.logger.error(err);
            });
            entry.createdAt = resultObj?.createdAt;
          }
        }
      }

      tags = this.filterList(tags, filter);
      const rows = tags.length;
      if (orderBy == undefined) {
        tags = this.sortList(tags, 'tag', 'DESC');
      } else {
        tags = this.sortList(tags, orderBy[0], orderBy[1]);
      }

      if (limit > 0) {
        tags = tags.slice(offset, offset + limit);
      }

      resolve({ tags, rows, name });
    });
  }

  async getTagHistory(query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const registryName = query.name;
      if (registryName == undefined || !registryName.length) {
        return reject(new HttpException('no registry name/path provided', 420));
      }
      const registryTag = query.tag;
      if (registryTag == undefined || !registryTag.length) {
        return reject(new HttpException('no tag provided', 420));
      }

      let errorState: any;
      const resultObj: any = {};
      const promises = [];

      const shuffledHosts = this.getShuffledRegistryHosts();
      const racePromises1 = [];
      for (const host of shuffledHosts) {
        racePromises1.push(
          firstValueFrom(
            this.httpService.get(
              host + '/v2/' + registryName + '/manifests/' + registryTag,
              {
                withCredentials: true,
                timeout: this.httpCallTimeout,
                auth: {
                  username: this.registryBasicAuthUsername,
                  password: this.registryBasicAuthPassword,
                },
              },
            ),
          ).then((response) => {
            const history = response.data.history;
            for (const entry of history) {
              const keys = Object.keys(entry);
              const value = entry[keys[0]];
              if (typeIsJSON(value)) {
                const parsed = JSON.parse(value);
                if (!(parsed.architecture == undefined)) {
                  resultObj.architecture = parsed.architecture;
                }
                if (
                  resultObj.createdAt == undefined &&
                  !(parsed.created == undefined)
                ) {
                  resultObj.createdAt = parsed.created;
                } else if (
                  !(resultObj.createdAt == undefined) &&
                  !(parsed.created == undefined) &&
                  new Date(resultObj.createdAt) > new Date(parsed.created)
                ) {
                  resultObj.createdAt = parsed.created;
                }
                if (
                  resultObj.updatedAt == undefined &&
                  !(parsed.created == undefined)
                ) {
                  resultObj.updatedAt = parsed.created;
                } else if (
                  !(resultObj.updatedAt == undefined) &&
                  !(parsed.created == undefined) &&
                  new Date(resultObj.updatedAt) < new Date(parsed.created)
                ) {
                  resultObj.updatedAt = parsed.created;
                }
                if (!(parsed.config?.Labels == undefined)) {
                  if (
                    !(
                      parsed.config?.Labels[
                        'org.opencontainers.image.ref.name'
                      ] == undefined
                    )
                  ) {
                    resultObj.osName =
                      parsed.config?.Labels[
                        'org.opencontainers.image.ref.name'
                      ];
                  }
                  if (
                    !(
                      parsed.config?.Labels[
                        'org.opencontainers.image.version'
                      ] == undefined
                    )
                  ) {
                    resultObj.osVersion =
                      parsed.config?.Labels['org.opencontainers.image.version'];
                  }
                }
              }
            }
          }),
        );
      }
      promises.push(
        Promise.any(racePromises1).catch((err) => {
          errorState = err;
        }),
      );
      await Promise.all(promises).catch((err) => {
        errorState = err;
      });
      if (!(errorState == undefined)) {
        return reject(errorState);
      }
      resolve(resultObj);
    });
  }

  async getManifests(query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const registryName = query.name;
      if (registryName == undefined || !registryName.length) {
        return reject(new HttpException('no registry name/path provided', 420));
      }
      const registryTag = query.tag;
      if (registryTag == undefined || !registryTag.length) {
        return reject(new HttpException('no tag provided', 420));
      }

      let errorState: any;
      const resultObj: any = await this.getTagHistory(query).catch((err) => {
        errorState = err;
      });
      const promises = [];

      const shuffledHosts = this.getShuffledRegistryHosts();
      const racePromises2 = [];
      for (const host of shuffledHosts) {
        racePromises2.push(
          firstValueFrom(
            this.httpService.get(
              host + '/v2/' + registryName + '/manifests/' + registryTag,
              {
                withCredentials: true,
                timeout: this.httpCallTimeout,
                auth: {
                  username: this.registryBasicAuthUsername,
                  password: this.registryBasicAuthPassword,
                },
                headers: {
                  Accept:
                    'application/vnd.docker.distribution.manifest.v2+json',
                },
              },
            ),
          ).then((response) => {
            if (!(response.headers?.['docker-content-digest'] == undefined)) {
              resultObj.digest = response.headers?.['docker-content-digest'];
            }
            resultObj.size = 0;
            resultObj.blobs = [];
            if (!(response.data == undefined)) {
              if (!(response.data?.config?.size == undefined)) {
                resultObj.size += parseInt(response.data?.config?.size, 10);
              }
              if (!(response.data?.layers == undefined)) {
                for (const layer of response.data?.layers) {
                  if (!(layer.size == undefined)) {
                    resultObj.size += parseInt(layer.size, 10);
                  }
                  if (!(layer.digest == undefined)) {
                    resultObj.blobs.push(layer.digest);
                  }
                }
              }
            }
          }),
        );
      }
      promises.push(
        Promise.any(racePromises2).catch((err) => {
          errorState = err;
        }),
      );
      await Promise.all(promises);

      if (!(errorState == undefined)) {
        this.logger.error(
          errorState.response?.data?.errors || errorState,
          undefined,
          this.loggerContext + ' - get manifests',
        );
        return reject(new HttpException('Manifest retrieval failed', 500));
      }

      resolve(resultObj);
    });
  }

  async deleteImage(query: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const registryName = query.name;
      if (registryName == undefined || !registryName.length) {
        return reject(new HttpException('no registry name/path provided', 420));
      }
      const tag = query.tag;
      if (tag == undefined || !tag.length) {
        return reject(new HttpException('no tag provided', 420));
      }

      // does mark blobs for soft deletion and will auto prune them within interval time (default 24h)
      let errorState: any;
      const raceRequests = [];
      const host = this.getShuffledRegistryHosts()?.[0];
      if (host == undefined) {
        return reject(
          new HttpException('deletion failed, no registry available', 404),
        );
      }

      const resultObj = (await firstValueFrom(
        this.httpService.get(
          host + '/v2/' + registryName + '/manifests/' + tag,
          {
            withCredentials: true,
            timeout: this.httpCallTimeout,
            auth: {
              username: this.registryBasicAuthUsername,
              password: this.registryBasicAuthPassword,
            },
            headers: {
              Accept: 'application/vnd.docker.distribution.manifest.v2+json',
            },
          },
        ),
      ).catch((err) => {
        errorState = err;
      })) as any;
      if (errorState == undefined) {
        // TESTING
        // console.info({ tag, data: resultObj?.data?.layers, headers: resultObj?.headers });
        const digest = resultObj?.headers?.['docker-content-digest'];
        if (!(digest == undefined)) {
          await firstValueFrom(
            this.httpService.delete(
              host + '/v2/' + registryName + '/manifests/' + digest,
              {
                withCredentials: true,
                timeout: this.httpCallTimeout,
                auth: {
                  username: this.registryBasicAuthUsername,
                  password: this.registryBasicAuthPassword,
                },
              },
            ),
          ).catch((err) => {
            errorState = err;
          });
        }
      }

      if (!(errorState == undefined)) {
        this.logger.error(
          errorState.response?.data?.errors || errorState,
          undefined,
          this.loggerContext + ' - delete image',
        );
        return reject(new HttpException('deletion failed', 500));
      }
      resolve({ message: 'image removed' });
    });
  }

  private sortList(list: any[], sortBy: string, sortDirection: string): any[] {
    return list.sort((a: any, b: any) => {
      if (sortDirection.toLowerCase() === 'asc') {
        if (a[sortBy] > b[sortBy]) return 1;
        if (a[sortBy] < b[sortBy]) return -1;
      } else {
        if (a[sortBy] > b[sortBy]) return -1;
        if (a[sortBy] < b[sortBy]) return 1;
      }
      return 0;
    });
  }

  private filterList(list: any[], filter: string): any[] {
    if (!(filter == undefined) && filter.length) {
      return list.filter((element) => {
        if (element.name?.toLowerCase().indexOf(filter.toLowerCase()) > -1) {
          return true;
        }
        if (element.tag?.toLowerCase().indexOf(filter.toLowerCase()) > -1) {
          return true;
        }
      });
    }
    return list;
  }

  private getLocallyCachedTags(repoName: string): { [tag: string]: string } {
    let folderContent = {};
    let targetPath = path.join(
      process.cwd(),
      'registry/docker/registry/v2/repositories',
      repoName,
      '_manifests/tags',
    );

    // TESTING
    // console.info('targetPath', targetPath);

    if (fs.existsSync(targetPath)) {
      const tagList = fs.readdirSync(targetPath);
      for (const tag of tagList) {
        folderContent[tag] = fs.statSync(path.join(targetPath, tag));
      }
    }

    // TESTING
    // console.info('folderContent', folderContent);

    return folderContent;
  }

  private getShuffledRegistryHosts(): string[] {
    return this.registryHosts.sort(() => 0.5 - Math.random());
  }
}
