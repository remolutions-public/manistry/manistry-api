import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { RegistryController } from './registry.controller';
import { RegistryService } from './registry.service';
import { GlobalProvidersModule } from '../../providers/global-providers.module';

const MODULES = [
  GlobalProvidersModule,
  HttpModule,
];

const CONTROLLERS = [
  RegistryController,
];

const SERVICES = [
  RegistryService,
];

@Module({
  imports: [...MODULES],
  controllers: [...CONTROLLERS],
  providers: [...SERVICES],
  exports: [],
})
export class RegistryModule { }
