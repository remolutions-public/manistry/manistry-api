import {
  Controller,
  Get,
  Post,
  Delete,
  Patch,
  Res,
  Body,
  Req,
  Param,
  Query,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiVersion } from '../../providers/guards/api-versions';
import { AuthService } from './auth.service';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Get()
  @ApiVersion()
  async refresh(@Req() req: any, @Res() res: any): Promise<any> {
    return await this.authService.refresh(req, res);
  }

  @Get('tokenconfig')
  @ApiVersion()
  async getTokenConfig(): Promise<any> {
    return await this.authService.getTokenConfig();
  }

  @Get('recaptcha')
  @ApiVersion()
  async getRecaptchaPublicKey(): Promise<any> {
    return await this.authService.getRecaptchaPublicKey();
  }

  @Post()
  @ApiVersion()
  async login(
    @Req() req: any,
    @Res() res: any,
    @Body() body: any,
  ): Promise<any> {
    return await this.authService.login(req, res, body);
  }

  @Delete('/:username')
  @ApiVersion()
  async logout(
    @Res() res: any,
    @Req() req: any,
    @Param() param: any,
  ): Promise<any> {
    return this.authService.logout(res, req, param);
  }
}
