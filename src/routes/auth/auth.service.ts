import { Injectable, Logger } from '@nestjs/common';
import { BcryptService } from '../../providers/services/bcrypt/bcrypt.service';
import { JwtService } from '../../providers/services/jwt/jwt.service';
import { ErrorService } from '../../providers/services/error/error.service';
import { GlobalConfigService } from '../../providers/services/config/config.service';
import { RecaptchaService } from '../../providers/services/recaptcha/recaptcha.service';
import { UserDto } from '../../dtos/user.dto';
import { parseBoolean } from '../../libraries/parser.lib';
import * as fs from 'fs';
import * as path from 'path';

@Injectable()
export class AuthService {
  private loggerContext = AuthService.name;
  private environment = this.globalConfigService.getValue('ENV');

  constructor(
    private readonly jwtService: JwtService,
    private readonly globalConfigService: GlobalConfigService,
    private readonly logger: Logger,
    private readonly recaptchaService: RecaptchaService,
    private readonly bcrypt: BcryptService,
  ) {}

  /**
   *
   * @param req
   * @param res
   */
  async refresh(req: any, res: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const tokens = await this.jwtService
        .verifyAuth(req)
        .catch(async (err) => {
          // does csrf token exist but auth token is missing?
          if (err.code === 420) {
            this.logger.error('refresh - forced auth clear');
            await this.logout(res, req, undefined, 'forced auth clear');
            return resolve(undefined);
          } else {
            return reject({
              error: err.error,
              status: err.code,
            });
          }
        });
      if (tokens == undefined || tokens.auth == undefined) {
        return reject(new Error('No tokens provided'));
      }
      const username = tokens.auth.username;
      const userObj = await this.getUserFromFileStorage(username);
      if (userObj == undefined) {
        return reject(new Error('refresh; user not found'));
      }

      const authToken = await this.makeAuthToken(userObj).catch((err) =>
        reject(err),
      );
      if (authToken == undefined) {
        return reject(new Error('Could not create auth token'));
      }

      const expiration = this.getAuthExpiration(authToken);
      const csrfToken = await this.makeCsrfToken(userObj).catch((err) =>
        reject(err),
      );

      res.cookie(
        this.jwtService.authTokenPrefix + '-' + userObj['username'],
        authToken,
        {
          ...this.cookieOptions(expiration),
        },
      );
      res.send({
        username: userObj['username'],
        isadmin: userObj['isadmin'],
        tokenExpiration: expiration,
        csrfToken,
      });
      resolve(undefined);
    }).catch((err) => ErrorService.getInst().handle(err, 401));
  }

  async getTokenConfig(): Promise<any> {
    return {
      csrfTokenName: this.jwtService.csrfTokenPrefix,
    };
  }

  /**
   *
   */
  async getRecaptchaPublicKey(): Promise<any> {
    const pubKey = this.globalConfigService.getValue('RECAPTCHA_PUBLIC_KEY');
    return {
      recaptchaSiteKey: pubKey,
    };
  }

  /**
   *
   * @param req
   * @param res
   * @param body
   */
  async login(req: any, res: any, body: any): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (this.environment !== 'DEV') {
        const recaptchaValid = await this.recaptchaService
          .verifyRecaptcha(req, body['recaptcha'])
          .catch((err) => reject(err));
        if (recaptchaValid == undefined) {
          return reject(new Error('reCAPTCHA failed to validate'));
        }
      }

      const user = body['username'];
      const pw = body['password'];

      const userObj = await this.getUserFromFileStorage(user);

      // DEBUGGING
      // this.logger.debug(user, 'user - login #0');
      // this.logger.debug(userObj, 'userObj - login #1');

      if (userObj == undefined) {
        return reject(new Error('user not found'));
      } else if (
        !(userObj == undefined) &&
        (userObj['password'] == undefined || !userObj['password'].length)
      ) {
        return reject(new Error('no password set, cannot login'));
      }

      if (!this.bcrypt.isValidHash(userObj.password)) {
        const hashedPw = this.bcrypt.getHash(userObj.password);
        userObj.password = hashedPw;
      }
      userObj.lastLogin = new Date().toISOString();

      if (this.bcrypt.checkHash(pw, userObj['password'])) {
        const authToken = await this.makeAuthToken(userObj).catch((err) =>
          reject(err),
        );
        if (authToken == undefined) {
          reject(new Error('Could not create auth token'));
          return;
        }

        // DEBUGGING
        // this.logger.debug(authToken, 'authToken - login #2');
        // this.logger.debug(userObj, 'userObj - login #3');

        const expiration = this.getAuthExpiration(authToken);
        const csrfToken = await this.makeCsrfToken(userObj).catch((err) =>
          reject(err),
        );

        // DEBUGGING
        // this.logger.debug(csrfToken, 'csrfToken - login #4');

        await this.writeUserFileStorage(userObj).catch((err) => {
          this.logger.error(err, undefined, this.loggerContext);
        });

        res.cookie(
          this.jwtService.authTokenPrefix + '-' + userObj['username'],
          authToken,
          {
            ...this.cookieOptions(expiration),
          },
        );

        const responseObj = {
          username: userObj['username'],
          isadmin: userObj['isadmin'],
          token_expiration: expiration,
          csrfToken,
        };

        // DEBUGGING
        // this.logger.debug(responseObj, this.loggerContext);

        res.send(responseObj);
        resolve(undefined);
      } else reject(new Error('login failed'));
    }).catch((err) => ErrorService.getInst().handle(err, 401));
  }

  /**
   * Removes the session cookie
   * @param res
   * @param req
   * @param param
   */
  async logout(res: any, req: any, param: any, customMessage?: string) {
    if (
      !(param == undefined) &&
      !(param.username == undefined) &&
      param.username.length &&
      param.username !== 'all'
    ) {
      res.clearCookie(this.jwtService.authTokenPrefix + '-' + param.username);
    } else {
      for (let key of Object.keys(req.cookies)) {
        if (key.indexOf(this.jwtService.authTokenPrefix) > -1) {
          res.clearCookie(key);
        }
      }
    }
    res.clearCookie(this.jwtService.csrfTokenPrefix);
    res.send({
      message: !(customMessage == undefined)
        ? customMessage
        : 'logout successful',
    });
  }

  ////////////// HELPER FUNCTIONS ////////////////
  /**
   *
   * @param expiration
   */
  private cookieOptions(expiration: number) {
    const opts = {
      expires: new Date(expiration),
      httpOnly: true,
      sameSite: 'Strict',
    };
    if (this.environment !== 'DEV') {
      opts['secure'] = true;
    }
    return opts;
  }

  /**
   *
   * @param authToken
   */
  private getAuthExpiration(authToken: string) {
    return new Date(
      this.jwtService.simpleBase64Decode(authToken)['exp'] * 1000,
    ).valueOf();
  }

  /**
   *
   * @param userObj
   */
  private async makeCsrfToken(userObj: UserDto) {
    return await this.jwtService.makeCsrfToken({
      username: userObj?.['username'],
    });
  }

  /**
   *
   * @param userObj
   */
  private async makeAuthToken(userObj: UserDto) {
    return await this.jwtService.makeAuthToken({
      username: userObj['username'],
      isadmin: userObj['isadmin'],
    });
  }

  static isOwnerUserOrAdmin(req: any, username: string): boolean {
    if (
      !(req.decoded_tokens == undefined) &&
      !(req.decoded_tokens.auth == undefined) &&
      !(req.decoded_tokens.auth.username == undefined) &&
      username !== req.decoded_tokens.auth.username &&
      !req.decoded_tokens.auth.isadmin
    ) {
      return false;
    }
    return true;
  }

  async getUserFromFileStorage(username: string): Promise<UserDto> {
    const userJsonFile = path.join(process.cwd(), 'secrets', 'users.json');
    try {
      if (!fs.existsSync(userJsonFile)) {
        fs.writeFileSync(userJsonFile, JSON.stringify({}));
        return;
      }
      const userJsonBuffer = fs.readFileSync(userJsonFile);
      const userJsonData = JSON.parse(userJsonBuffer.toString());
      return userJsonData?.[username];
    } catch (e) {
      this.logger.error(e, undefined, 'AuthService - getUserFromFileStorage');
      return;
    }
  }

  async writeUserFileStorage(userObj: any): Promise<boolean> {
    const doUpdateUsers = parseBoolean(
      this.globalConfigService.getValue('DO_UPDATE_USERS'),
    );
    if (!doUpdateUsers) {
      return true;
    }
    const username = userObj.username;
    if (username == undefined || !username.length) {
      return false;
    }
    const userJsonFile = path.join(process.cwd(), 'secrets', 'users.json');
    try {
      if (!fs.existsSync(userJsonFile)) {
        fs.writeFileSync(
          userJsonFile,
          JSON.stringify({
            [username]: userObj,
          }),
        );
        return true;
      }
      const userJsonBuffer = fs.readFileSync(userJsonFile);
      const userJsonData = JSON.parse(userJsonBuffer.toString());
      if (userJsonData == undefined) {
        return false;
      }
      let userJsonObj = userJsonData[username];
      if (userJsonObj == undefined) {
        userJsonObj = {};
      }
      Object.assign(userJsonObj, userObj);
      userJsonData[username] = userJsonObj;
      fs.writeFileSync(userJsonFile, JSON.stringify(userJsonData));
      return true;
    } catch (e) {
      this.logger.error(e, undefined, 'AuthService - writeUserFileStorage');
      return false;
    }
  }
}
