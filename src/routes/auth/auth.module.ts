import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { GlobalProvidersModule } from '../../providers/global-providers.module';

const MODULES = [
  GlobalProvidersModule,
  HttpModule,
];

const CONTROLLERS = [AuthController];

const SERVICES = [AuthService];

@Module({
  imports: [...MODULES],
  controllers: [...CONTROLLERS],
  providers: [...SERVICES],
  exports: [],
})
export class AuthModule {}
