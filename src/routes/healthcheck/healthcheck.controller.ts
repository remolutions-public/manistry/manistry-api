import { Controller, Get } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { ApiVersion } from '../../providers/guards/api-versions';
import { HealthCheckService } from './healthcheck.service';
import { ErrorService } from '../../providers/services/error/error.service';

@ApiTags('healthcheck')
@Controller('healthcheck')
export class HealthCheckController {
  constructor(
    private readonly healthCheckService: HealthCheckService,
    private readonly errorService: ErrorService
  ) { }

  @Get()
  @ApiVersion()
  async getHealthCheck(): Promise<any> {
    const result = await this.healthCheckService.getHealthCheck();
    if (result == undefined) {
      return true;
    }
    this.errorService.handle(result, 500);
  }
}
