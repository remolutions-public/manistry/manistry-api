import { Injectable, Logger } from '@nestjs/common';
import { CleanerAgent } from '../../providers/agents/cleaner/cleaner.agent';

@Injectable()
export class HealthCheckService {
  constructor(
    private readonly logger: Logger,
    private readonly cleanerAgent: CleanerAgent,
  ) {}

  async getHealthCheck(): Promise<{ error: string }> {
    if (
      new Date().valueOf() - this.cleanerAgent.lastAgentStartTime >
      this.cleanerAgent.maxAllowedAgentRunDelay
    ) {
      return { error: 'cleaner agent not running' };
    }
    return undefined;
  }
}
