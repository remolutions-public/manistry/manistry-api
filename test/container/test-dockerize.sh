#!/bin/sh

IMAGE_VERSION="0.0.3-arm64"
IMAGE_NAME=test-container
IMAGE_REGISTRY=localhost:5000
IMAGE_PATH=$IMAGE_REGISTRY/test/$IMAGE_NAME:$IMAGE_VERSION


BASE_PATH=$(dirname \"$0\" | tr -d '"')
sourceEnvFile() {
  if [ -f "$BASE_PATH/../../.env.dev" ]
  then
    export $(cat $BASE_PATH/../../.env.dev | xargs)
  else
    echo "$BASE_PATH/../../.env.dev not found, aborting..."
    exit 1
  fi
}
sourceEnvFile


echo "$BASIC_AUTH_PASSWORD" | docker login $IMAGE_REGISTRY -u $BASIC_AUTH_USERNAME --password-stdin
# docker build -t "$IMAGE_PATH" -f $BASE_PATH/Dockerfile $BASE_PATH
docker buildx build --platform linux/arm64 -t "$IMAGE_PATH" -f $BASE_PATH/Dockerfile --build-arg=VERSION_ARG=$IMAGE_VERSION --push $BASE_PATH
# docker push "$IMAGE_PATH"


echo "pushed image $IMAGE_PATH"
