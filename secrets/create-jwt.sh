#!/bin/sh
BASE_PATH=$(dirname \"$0\" | tr -d '"')

JWT_PATH=$BASE_PATH/jwt
JWT_PRIV_KEY=private_key.jwt
JWT_PUB_KEY=public_key.jwt
mkdir -p "$JWT_PATH"
openssl ecparam -name prime256v1 -genkey -noout -out "$JWT_PATH/$JWT_PRIV_KEY"
openssl ec -in "$JWT_PATH/$JWT_PRIV_KEY" -pubout -out "$JWT_PATH/$JWT_PUB_KEY"

exit 0
