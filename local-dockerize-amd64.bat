set IMAGE_VERSION=v1.0.3
set IMAGE_ARCH=amd64
set IMAGE_PATH="registry.gitlab.com/remolutions-public/manistry/manistry-api/manistry-api"


FOR /F "" %%g IN ('docker ps -a -q --filter="name=%IMAGE_PATH%"') do (SET CONTAINER_CHECK_RESULT=%%g)
if [%CONTAINER_CHECK_RESULT%]==[] (
  echo no container found, skipping...
) else (
  echo removing container %CONTAINER_CHECK_RESULT%
  docker rm -f %CONTAINER_CHECK_RESULT%
)


docker build -t %IMAGE_PATH%:%IMAGE_VERSION%-%IMAGE_ARCH% -f "Dockerfile" .
docker push %IMAGE_PATH%:%IMAGE_VERSION%-%IMAGE_ARCH%


@REM pause
