#!/bin/sh

# correct for actual prod build tag (e. g. v0.0.0)
ENV=$CI_ENVIRONMENT_NAME
if [ -z "$ENV" ]; then
  ENV=STAGING
fi
LOWERCASE_ENV=$(echo "$ENV" | tr '[:upper:]' '[:lower:]')

IMAGE_TAG=$CI_PIPELINE_ID-$ENV
if [ "$ENV" = "PROD" ]; then
  IMAGE_TAG=$CI_COMMIT_TAG
fi

BASE_PATH=$(dirname \"$0\" | tr -d '"')

echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" | tr -d '\n' > /kaniko/.docker/config.json


IMAGE_PATH=$CI_REGISTRY/$CI_PROJECT_PATH/$CI_PROJECT_NAME


/kaniko/executor  --context $BASE_PATH \
                  --dockerfile $BASE_PATH/Dockerfile \
                  --destination "$IMAGE_PATH:$IMAGE_TAG" \
                  --destination "$IMAGE_PATH:latest"
if [ "$?" = 1 ]; then
  exit 1
fi


echo "pushed images $IMAGE_PATH:$IMAGE_TAG and $IMAGE_PATH:latest"
