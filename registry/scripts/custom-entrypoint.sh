#!/bin/sh

sh -c "sh /garbage-collection.sh" &


mkdir -p /etc/registry/basic-auth/
apk add apache2-utils
htpasswd -Bbn $BASIC_AUTH_USERNAME "$BASIC_AUTH_PASSWORD" > /etc/registry/basic-auth/htpasswd
export REGISTRY_HTTP_SECRET=$(cat /etc/registry/basic-auth/htpasswd)


cp -f /etc/docker/registry/tmp/config.yml /etc/docker/registry/config.yml 
sed -i 's|#PROXY_URL#|'"$PROXY_URL"'|g' /etc/docker/registry/config.yml 


sh -c "/entrypoint.sh /etc/docker/registry/config.yml"
