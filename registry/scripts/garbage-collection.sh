#!/bin/sh

sleep 30

while [ true ]
do
  /bin/registry garbage-collect --delete-untagged=true --dry-run=false /etc/docker/registry/config.yml &> /dev/null
  echo "[$(date -u +"%Y-%m-%d")] garbage collection..."
  sleep 7200
done
