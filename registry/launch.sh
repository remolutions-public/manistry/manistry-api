#!/bin/sh


NETWORK_NAME=manistry-net
BASE_PATH=$(dirname \"$0\" | tr -d '"')


if [ -z "$(docker network ls --filter "name=$NETWORK_NAME" --quiet)" ]; then
  docker network create $NETWORK_NAME
fi


if [ -f $BASE_PATH/../.env.dev ]; then
  export $(xargs <$BASE_PATH/../.env.dev)
else
  echo "$BASE_PATH/../.env.dev not found"
  exit 1
fi


if [ -z "$BASIC_AUTH_USERNAME" ] || [ -z "$BASIC_AUTH_PASSWORD" ]; then
  echo "BASIC_AUTH_PASSWORD or BASIC_AUTH_USERNAME not provided, aborting..."
  exit 1
fi


mkdir -p $BASE_PATH/data


cd $BASE_PATH
if [ "$1" = "--attached" ]; then
  env
  docker-compose up
else
  docker-compose up -d
fi
