set DOCKER_CONTAINER_NAME=manistry-api
set ENV=DEV
set APP_PORT=3030
set API_VERSION=test
set REGISTRY_HOSTS=http://localhost:5000
set BASIC_AUTH_USERNAME=registry
set BASIC_AUTH_PASSWORD=test
set JWT_PRIV_KEY=private_key.jwt
set JWT_PUB_KEY=public_key.jwt
set RECAPTCHA_PRIVATE_KEY=test
set RECAPTCHA_PUBLIC_KEY=test
set RECAPTCHA_VERIFY_ADDRESS=https://www.google.com/recaptcha/api/siteverify



FOR /F "" %%g IN ('docker ps -a -q --filter="name=%DOCKER_CONTAINER_NAME%"') do (SET CONTAINER_CHECK_RESULT=%%g)
if [%CONTAINER_CHECK_RESULT%]==[] (
  echo no container found, skipping...
) else (
  echo removing container %CONTAINER_CHECK_RESULT%
  docker rm -f %CONTAINER_CHECK_RESULT%
)


docker run -d --name %DOCKER_CONTAINER_NAME% ^
            -e ENV=%ENV% ^
            -e APP_PORT=%APP_PORT% ^
            -e API_VERSION=%API_VERSION% ^
            -e REGISTRY_HOSTS=%REGISTRY_HOSTS% ^
            -e BASIC_AUTH_USERNAME=%BASIC_AUTH_USERNAME% ^
            -e BASIC_AUTH_PASSWORD=%BASIC_AUTH_PASSWORD% ^
            -e JWT_PRIV_KEY=%JWT_PRIV_KEY% ^
            -e JWT_PUB_KEY=%JWT_PUB_KEY% ^
            -e RECAPTCHA_PRIVATE_KEY=%RECAPTCHA_PRIVATE_KEY% ^
            -e RECAPTCHA_PUBLIC_KEY=%RECAPTCHA_PUBLIC_KEY% ^
            -e RECAPTCHA_VERIFY_ADDRESS=%RECAPTCHA_VERIFY_ADDRESS% ^
            -p %APP_PORT%:%APP_PORT% ^
            %DOCKER_CONTAINER_NAME%:latest
